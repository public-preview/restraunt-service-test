import { ApolloServer, gql } from "apollo-server-express";
import { createTestClient } from "apollo-server-testing";
import { DocumentNode } from "graphql";
import imageServiceApi from "../../src/api/image.service.api";
import { schema } from "../../src/graphql/schema";
import restaurantRepository from "../../src/repositories/restaurant.repository";
import { RestaurantService } from "../../src/services/restaurant.service";
import { IApolloContexts } from "../../src/types/context";

jest.mock("../../src/api/image.service.api", () => ({
  __esModule: true,
  default: {
    fetchImagesForRestaurants: jest.fn().mockResolvedValue([]),
  },
}));

jest.mock("../../src/repositories/restaurant.repository", () => ({
  __esModule: true,
  default: {
    fetchRestaurants: jest.fn().mockResolvedValue({
      results: [
        {
          restaurantUuid: "example-uuid",
          name: "Example Restaurant",
          country: { code: "FR", locales: [] },
          images: [],
          allowReview: true,
        },
      ],
      total: 1,
    }),
  },
}));

describe("Restaurant GraphQL API E2E Tests", () => {
  let query: (options: {
    query: DocumentNode;
    variables?: Record<string, any>;
  }) => Promise<any>;

  beforeAll(() => {
    const restaurantService = new RestaurantService(
      restaurantRepository,
      imageServiceApi
    );
    const server = new ApolloServer({
      schema,
      context: (): IApolloContexts => ({ restaurantService }),
    });

    const testClient = createTestClient(server);
    query = testClient.query;
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should fetch restaurants without images", async () => {
    const SEARCH_RESTAURANTS: DocumentNode = gql`
      query SearchRestaurants(
        $name: String
        $withImages: Boolean
        $page: Int
        $pageSize: Int
      ) {
        searchRestaurants(
          name: $name
          withImages: $withImages
          page: $page
          pageSize: $pageSize
        ) {
          restaurants {
            restaurantUuid
            name
            country {
              code
              locales
            }
            images
            allowReview
          }
          pagination {
            total
            pageCount
            currentPage
          }
        }
      }
    `;

    restaurantRepository.fetchRestaurants = jest.fn().mockResolvedValue({
      results: [
        {
          restaurant_uuid: "example-uuid",
          name: "Example Restaurant",
          country_code: "FR",
        },
      ],
      total: 1,
    });

    imageServiceApi.fetchImagesForRestaurants = jest.fn().mockResolvedValue([]);

    const res = await query({
      query: SEARCH_RESTAURANTS,
      variables: { name: "Example", withImages: false, page: 1, pageSize: 10 },
    });

    expect(res.data.searchRestaurants.restaurants.length).toBe(1);
    expect(res.data.searchRestaurants).toEqual({
      pagination: {
        currentPage: 1,
        pageCount: 1,
        total: 1,
      },
      restaurants: [
        {
          allowReview: true,
          country: {
            code: "FR",
            locales: [],
          },
          images: [],
          name: "Example Restaurant",
          restaurantUuid: "example-uuid",
        },
      ],
    });
    expect(restaurantRepository.fetchRestaurants).toHaveBeenCalledWith({
      name: "Example",
      withImages: false,
      page: 1,
      pageSize: 10,
    });
    expect(imageServiceApi.fetchImagesForRestaurants).toHaveBeenCalled();
  });
});
