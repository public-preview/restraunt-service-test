import ImageServiceApi, {
  IImageServiceApi,
  Image,
} from "../../src/api/image.service.api";
import { CountryCode } from "../../src/models/country/enums/country.code";
import {
  Restaurant,
  RestaurantsArgs,
} from "../../src/models/restaurant/restaurant";
import RestaurantRepository, {
  IRestaurantRepository,
} from "../../src/repositories/restaurant.repository";
import { RestaurantService } from "../../src/services/restaurant.service";

jest.mock("../../src/repositories/restaurant.repository");
jest.mock("../../src/api/image.service.api");

describe("RestaurantService", () => {
  let service: RestaurantService;
  let mockImageService: jest.Mocked<IImageServiceApi>;
  let mockRestaurantRepository: jest.Mocked<IRestaurantRepository>;

  beforeEach(() => {
    jest.clearAllMocks();
    mockImageService = ImageServiceApi as any;
    mockRestaurantRepository = RestaurantRepository as any;
    service = new RestaurantService(mockRestaurantRepository, mockImageService);
  });

  describe("searchRestaurants", () => {
    it("should fetch restaurants without images", async () => {
      const mockRestaurants: Restaurant[] = [
        {
          restaurantUuid: "example-uuid",
          name: "Example Restaurant",
          country: {
            code: CountryCode.FR,
            locales: [],
          },
          images: [],
        },
      ];

      const mockImages: Image[] = [
        { imageUuid: "some image uuid", url: "random url" },
      ];

      const args: RestaurantsArgs = {
        withImages: false,
      };

      mockRestaurantRepository.fetchRestaurants.mockResolvedValue({
        results: mockRestaurants,
        total: mockRestaurants.length,
      });
      mockImageService.fetchImagesForRestaurants.mockResolvedValue(mockImages);

      const result = await service.searchRestaurants(args);

      expect(mockRestaurantRepository.fetchRestaurants).toHaveBeenCalledWith(
        expect.objectContaining(args)
      );
      expect(mockImageService.fetchImagesForRestaurants).toHaveBeenCalled();
      expect(result).toEqual({
        restaurants: expect.any(Array),
        pagination: {
          total: mockRestaurants.length,
          pageCount: 1,
          currentPage: 1,
        },
      });
    });
  });
});
