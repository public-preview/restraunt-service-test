import config from "config";
import { Knex } from "knex";

interface KnexConfig {
  [key: string]: Knex.Config;
}

const dbConfig: KnexConfig = {
  development: {
    client: "pg",
    connection: config.get("database"),
  },
  test: {
    client: "pg",
    connection: config.get("database"),
  },
};

export default dbConfig;
