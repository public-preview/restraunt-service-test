import knex from "knex";
import config from "./knexfile";
const environment = process.env.NODE_ENV || "development";
const dbConfig = config[environment];

if (!dbConfig) {
  throw new Error(`Invalid NODE_ENV set: ${environment}. KnexConfig not found. Add new config to knexfile.js`);
}

const db = knex(dbConfig);

export default db;
