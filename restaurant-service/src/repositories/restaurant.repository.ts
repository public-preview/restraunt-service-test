import { Knex } from "knex";
import knexInstance from "../db/index";
import { RestaurantsArgs } from "../models/restaurant/restaurant";

type TotalCountResult = {
  total: number;
};

export interface IRestaurantRepository {
  fetchRestaurants(args: RestaurantsArgs): Promise<{
    results: any;
    total: number;
  }>;
}

class RestaurantRepository implements IRestaurantRepository {
  async fetchRestaurants({
    name,
    withImages,
    page = 1,
    pageSize = 10,
  }: RestaurantsArgs): Promise<{
    results: any;
    total: number;
  }> {
    const query = this.buildBaseQuery();
    this.applyFilters(query, { name, withImages });

    const [results, totalResult] = await Promise.all([
      query.offset((page - 1) * pageSize).limit(pageSize),
      this.countTotal({ name, withImages }),
    ]);

    const total = parseInt(totalResult.total.toString(), 10);
    return { results, total };
  }

  private buildBaseQuery(): Knex.QueryBuilder {
    return knexInstance("restaurant")
      .select(
        "restaurant.restaurant_uuid",
        "restaurant.name",
        "country.country_code",
        "country.locales",
        knexInstance.raw("array_agg(restaurant_has_image.image_uuid) as images")
      )
      .leftJoin("country", "restaurant.country_code", "country.country_code")
      .leftJoin(
        "restaurant_has_image",
        "restaurant.restaurant_uuid",
        "restaurant_has_image.restaurant_uuid"
      )
      .groupBy("restaurant.restaurant_uuid", "country.country_code");
  }

  private applyFilters(
    query: Knex.QueryBuilder,
    { name, withImages }: { name?: string; withImages?: boolean }
  ) {
    if (name) {
      query.where("restaurant.name", "like", `%${name}%`);
    }
    if (withImages) {
      query.havingRaw("count(restaurant_has_image.image_uuid) > 0");
    }
  }

  private async countTotal({
    name,
    withImages,
  }: {
    name?: string;
    withImages?: boolean;
  }): Promise<TotalCountResult> {
    const query = knexInstance("restaurant")
      .countDistinct("restaurant.restaurant_uuid as total")
      .leftJoin("country", "restaurant.country_code", "country.country_code")
      .leftJoin(
        "restaurant_has_image",
        "restaurant.restaurant_uuid",
        "restaurant_has_image.restaurant_uuid"
      );

    if (name) {
      query.where("restaurant.name", "like", `%${name}%`);
    }
    if (withImages) {
      query.whereNotNull("restaurant_has_image.image_uuid");
    }

    const result = (await query.first()) as TotalCountResult;
    return result;
  }
}

export default new RestaurantRepository();
