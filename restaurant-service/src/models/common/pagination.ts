export interface Pagination {
  total: number;
  pageCount: number;
  currentPage: number;
}
