import { Pagination } from "../common/pagination";
import { Country } from "../country/country";

export interface Restaurant {
  restaurantUuid: string;
  name: string;
  country: Country;
  images?: string[];
}

export interface RestaurantsArgs {
  name?: string;
  withImages?: boolean;
  page?: number;
  pageSize?: number;
}

export interface RestaurantResponse {
  restaurants: Restaurant[];
  pagination: Pagination;
}
