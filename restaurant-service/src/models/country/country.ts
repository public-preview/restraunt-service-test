import { CountryCode } from "./enums/country.code";

export interface Country {
  code: CountryCode;
  locales: string[];
}
