import { ApolloError } from "apollo-server-express";
import { IImageServiceApi, Image } from "../api/image.service.api";
import { CountryCode } from "../models/country/enums/country.code";
import {
  Restaurant,
  RestaurantResponse,
  RestaurantsArgs,
} from "../models/restaurant/restaurant";
import { IRestaurantRepository } from "../repositories/restaurant.repository";

export interface IRestaurantService {
  searchRestaurants(args: RestaurantsArgs): Promise<RestaurantResponse>;
}

export class RestaurantService implements IRestaurantService {
  private repository: IRestaurantRepository;
  private imageServiceApi: IImageServiceApi;

  constructor(
    repository: IRestaurantRepository,
    imageServiceApi: IImageServiceApi
  ) {
    this.repository = repository;
    this.imageServiceApi = imageServiceApi;
  }

  async searchRestaurants(args: RestaurantsArgs): Promise<RestaurantResponse> {
    const { page = 1, pageSize = 10 } = args;
    try {
      const { results, total } = await this.repository.fetchRestaurants({
        ...args,
        page,
        pageSize,
      });
      const images = await this.imageServiceApi.fetchImagesForRestaurants();
      const restaurants = this.formatRestaurants(results);
      return this.formatResponse(restaurants, images, total, page, pageSize);
    } catch (error) {
      console.error("Error fetching restaurants:", error);
      throw new ApolloError(error.message, "RESTAURANT_FETCH_FAILED");
    }
  }

  private formatRestaurants(results: any[]): Restaurant[] {
    return results.map((item) => ({
      restaurantUuid: item.restaurant_uuid,
      name: item.name,
      country: {
        code: item.country_code,
        locales: item.locales ?? [],
      },
      images: item.images ? item.images.filter((id: string) => id != null) : [],
    }));
  }

  private formatResponse(
    restaurants: Restaurant[],
    images: Image[],
    total: number,
    currentPage: number,
    pageSize: number
  ) {
    const pageCount = Math.ceil(total / pageSize);
    return {
      restaurants: this.attachImages(restaurants, images),
      pagination: {
        total,
        pageCount,
        currentPage,
      },
    };
  }

  private attachImages(restaurants: Restaurant[], images: Image[]) {
    return restaurants.map((restaurant) => ({
      ...restaurant,
      images: images
        .filter((img) => restaurant.images?.includes(img.imageUuid))
        .map((img) => img.url),
      allowReview: restaurant.country.code === CountryCode.FR,
    }));
  }
}
