import { gql } from "apollo-server-express";

export const commonTypeDefs = gql`
  type Pagination {
    total: Int!
    pageCount: Int!
    currentPage: Int!
  }
`;
