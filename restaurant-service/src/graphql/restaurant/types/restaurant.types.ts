import { gql } from "apollo-server-express";

export const restaurantTypeDefs = gql`
  type Restaurant {
    restaurantUuid: String!
    name: String!
    country: Country!
    images: [String]
    allowReview: Boolean!
  }

  type RestaurantResponse {
    restaurants: [Restaurant]!
    pagination: Pagination!
  }

  type Query {
    searchRestaurants(
      name: String
      withImages: Boolean
      page: Int = 1
      pageSize: Int = 10
    ): RestaurantResponse
  }
`;
