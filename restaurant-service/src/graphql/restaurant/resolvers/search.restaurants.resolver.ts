import { IResolvers } from "apollo-server-express";
import {
  RestaurantResponse,
  RestaurantsArgs,
} from "../../../models/restaurant/restaurant";
import { IApolloContexts } from "../../../types/context";

const searchRestaurantsResolver: IResolvers<any, IApolloContexts> = {
  Query: {
    async searchRestaurants(
      _: unknown,
      args: RestaurantsArgs,
      { restaurantService }
    ): Promise<RestaurantResponse> {
      return restaurantService.searchRestaurants(args);
    },
  },
};

export default searchRestaurantsResolver;
