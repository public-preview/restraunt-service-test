import { gql } from "apollo-server-express";

export const countryTypeDefs = gql`
  enum Code {
    UK
    FR
    ES
  }

  type Country {
    code: Code!
    locales: [String]
  }
`;
