import { makeExecutableSchema } from "apollo-server-express";
import { commonTypeDefs } from "./common/types/pagination.type";
import { countryTypeDefs } from "./country/types/country.type";
import restaurantResolvers from "./restaurant/resolvers";
import { restaurantTypeDefs } from "./restaurant/types/restaurant.types";

const typeDefs = [commonTypeDefs, countryTypeDefs, restaurantTypeDefs];
const resolvers = [...restaurantResolvers];

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});
