import axios from "axios";
import config from "config";

export interface Image {
  imageUuid: string;
  url: string;
}

export interface IImageServiceApi {
  fetchImagesForRestaurants(): Promise<Image[]>;
}

class ImageServiceApi implements IImageServiceApi {
  async fetchImagesForRestaurants(): Promise<Image[]> {
    try {
      const response = await axios.get<{ images: Image[] }>(
        `${config.get("externalServices.imageService")}`
      );
      return response.data.images;
    } catch (error) {
      console.error('Unable to fetch images', error);
      throw new Error('Unable to fetch images');
    }
  }
}

export default new ImageServiceApi();
