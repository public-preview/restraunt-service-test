import { ApolloServer } from "apollo-server-express";
import config from "config";
import express from "express";
import imageServiceApi from "./api/image.service.api";
import { schema } from "./graphql/schema";
import restaurantRepository from "./repositories/restaurant.repository";
import { RestaurantService } from "./services/restaurant.service";

const main = async () => {
  const app = express();

  const server = new ApolloServer({
    schema,
    context: () => ({
      restaurantService: new RestaurantService(
        restaurantRepository,
        imageServiceApi
      ),
    }),
  });

  await server.start();

  server.applyMiddleware({ app });

  app.listen({ port: config.get("server.port") }, () =>
    console.info(
      `🚀 Server ready and listening at ==> http://localhost:${config.get(
        "server.port"
      )}${server.graphqlPath}`
    )
  );
};

main().catch((error) => {
  console.error("Server failed to start", error);
});
