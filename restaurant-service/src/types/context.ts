import {
  IRestaurantService
} from "../services/restaurant.service";

export interface IApolloContexts {
  restaurantService: IRestaurantService;
}
